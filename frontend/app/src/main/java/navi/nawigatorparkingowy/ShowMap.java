package navi.nawigatorparkingowy;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ShowMap extends AppCompatActivity {

    final Context context = this;
    private String call = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        call = intent.getStringExtra("call");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_map);

        new JSONTask().execute("http://192.168.0.14:8080/" + call);
    }

    public class JSONTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuilder buffer = new StringBuilder();

                String line;

                while((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                return buffer.toString();

            } catch(IOException e) {
                e.printStackTrace();
            } finally {
                if(connection != null) connection.disconnect();

                try {
                    if(reader != null) reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result == null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Informacja")
                        .setMessage("Brak wolnych miejsc! Przepraszamy i prosimy o ponowne wyszukanie miesjca.")
                        .setCancelable(false)
                        .setPositiveButton("Wróć", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                onBackPressed();
                            }
                        });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return;
            }

            Path pathCanvas = new Path();
            ImageView myImageView = (ImageView) findViewById(R.id.imageView);

            float scaleFactorX = 0;
            float originalImageWidth = 292.0f;
            if (myImageView != null) {
                scaleFactorX = myImageView.getWidth() / originalImageWidth;
            }

            float scaleFactorY = 0;
            float originalImageHeight = 483.0f;
            if (myImageView != null) {
                scaleFactorY = myImageView.getHeight() / originalImageHeight;
            }

            try {
                JSONObject info = new JSONObject(result);
                JSONArray path = info.getJSONArray("path");

                for(int i=0; i < path.length(); ++i) {
                    JSONObject tempObj = path.getJSONObject(i);

                    float x = tempObj.getInt("x") * scaleFactorX;
                    float y = tempObj.getInt("y") * scaleFactorY;

                    if(i==0) {
                        pathCanvas.moveTo(x, y);
                    } else {
                        pathCanvas.lineTo(x, y);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Bitmap bitmap = null;
            if (myImageView != null) {
                bitmap = Bitmap.createBitmap(myImageView.getWidth(), myImageView.getHeight(), Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = null;
            if (bitmap != null) {
                canvas = new Canvas(bitmap);
            }

            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

            paint.setColor(Color.RED);
            paint.setStrokeWidth(8.0f);
            paint.setStyle(Paint.Style.STROKE);

            if (canvas != null) {
                canvas.drawPath(pathCanvas, paint);
            }

            if (myImageView != null) {
                myImageView.setImageBitmap(bitmap);
            }
        }
    }
}