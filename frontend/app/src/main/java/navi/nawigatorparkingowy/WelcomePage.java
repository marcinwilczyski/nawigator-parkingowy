package navi.nawigatorparkingowy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class WelcomePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_page);

        Button smallPlaceBtn = (Button) findViewById(R.id.smallPlace);
        Button bigPlaceBtn = (Button) findViewById(R.id.bigPlace);

        if (smallPlaceBtn != null) {
            smallPlaceBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ShowMap.class);
                    intent.putExtra("call","park/normal");
                    startActivity(intent);
                }
            });
        }

        if (bigPlaceBtn != null) {
            bigPlaceBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ShowMap.class);
                    intent.putExtra("call","park/large");
                    startActivity(intent);
                }
            });
        }
    }
}
