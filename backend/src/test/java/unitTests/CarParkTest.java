package unitTests;

import DBEntities.ParkingNode;
import DBEntities.ParkingNodeConnection;
import DBEntities.ParkingPlace;
import DBRepositories.DBRepository;
import DBRepositories.IRepository;
import carParkModel.CarPark;
import carParkModel.PathNode;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Marcin on 19.05.2016.
 */
public class CarParkTest {
    IRepository repository = mock(DBRepository.class);
    @Before
    public void initRepository(){
        List<ParkingNodeConnection> connections = new ArrayList<ParkingNodeConnection>();
        connections.add(new ParkingNodeConnection(0, 1));
        connections.add(new ParkingNodeConnection(1, 2));
        connections.add(new ParkingNodeConnection(2, 3));

        connections.add(new ParkingNodeConnection(3, 4));
        connections.add(new ParkingNodeConnection(3, 5));
        connections.add(new ParkingNodeConnection(3, 6));

        connections.add(new ParkingNodeConnection(1, 7));
        when(repository.getParkingNodeConnections()).thenReturn(connections);

        List<ParkingNode> nodes = new ArrayList<ParkingNode>();
        nodes.add(new ParkingNode(0, 0, 0));
        nodes.add(new ParkingNode(1, 0, 1));
        nodes.add(new ParkingNode(2, 0, 2));
        nodes.add(new ParkingNode(3, 0, 3));

        nodes.add(new ParkingNode(7, 1, 1));

        nodes.add(new ParkingNode(4, -1, 3));
        nodes.add(new ParkingNode(5, 1, 3));
        nodes.add(new ParkingNode(6, 0, 4));


        when(repository.getParkingNodes()).thenReturn(nodes);

        List<ParkingPlace> places = new ArrayList<ParkingPlace>();
        places.add(new ParkingPlace(0, 4));
        places.add(new ParkingPlace(1, 5));
        places.add(new ParkingPlace(2, 6));
        places.add(new ParkingPlace(3, 7));

        when(repository.getParkingPlaces("big")).thenReturn(places);
    }
    @Test
    public void testPathFiding0() throws Exception{
        CarPark carPark = new CarPark(0, repository.getParkingNodes(), repository.getParkingNodeConnections(), repository.getParkingPlaces("big"));
        ArrayList<PathNode> nodes = new ArrayList<>();
        nodes.add(new PathNode(0,0));
        nodes.add(new PathNode(0,1));
        nodes.add(new PathNode(1,1));

        assertEquals(carPark.getPath().getPath(),nodes);
    }

    @Test
    public void testPathFiding1() throws Exception{
        CarPark carPark = new CarPark(1, repository.getParkingNodes(), repository.getParkingNodeConnections(), repository.getParkingPlaces("big"));
        ArrayList<PathNode> nodes = new ArrayList<>();
        nodes.add(new PathNode(0,1));
        nodes.add(new PathNode(1,1));

        assertEquals(carPark.getPath().getPath(),nodes);
    }

    @Test(expected = Exception.class)
    public void testPathFiding2() throws Exception{
        List<ParkingPlace> places = new ArrayList<ParkingPlace>();
        when(repository.getParkingPlaces("big")).thenReturn(places);
        CarPark carPark = new CarPark(0, repository.getParkingNodes(), repository.getParkingNodeConnections(), repository.getParkingPlaces("big"));

        carPark.getPath();
    }
}
