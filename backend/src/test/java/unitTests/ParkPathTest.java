package unitTests;

import DBEntities.ParkingNode;
import carParkModel.ParkPath;
import carParkModel.PathNode;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;


/**
 * Created by Marcin on 05.05.2016.
 */
public class ParkPathTest {
    @Test
    public void testClone() throws Exception {
        ParkPath path = new ParkPath();
        path.addNode(new PathNode(200,300));
        ParkPath newPath = path.clone();
        assertEquals(path.getPath(), newPath.getPath());
    }

    @Test
    public void testAdd(){
        ArrayList<PathNode> nodes = new ArrayList<>();
        nodes.add(new PathNode(200,300));
        nodes.add(new PathNode(400,300));
        nodes.add(new PathNode(600,300));
        ParkPath path = new ParkPath();
        for (PathNode node:nodes ) {
            path.addNode(node);
        }

        assertEquals(path.getPath(), nodes);
    }
}