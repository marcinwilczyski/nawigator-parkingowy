package nawigator;

/**
 * Created by Marcin on 31.03.2016.
 */
import DBRepositories.IRepository;
import DBRepositories.MockedRepository;
import DBRepositories.MockedRepositoryToMap;
import carParkModel.ParkPath;
import carParkModel.CarPark;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import DBEntities.*;
import DBRepositories.DBRepository;
import carParkModel.PathNode;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class ParkController {

   // @RequestMapping("/park/{size}")
    @RequestMapping(value = "/park/{size}", method = GET)
    @ResponseBody
    public ParkPath park(@PathVariable String size) throws Exception {
        
        IRepository repository = new DBRepository(); //tutaj podmień MockedRepository na klasę którą napiszesz. Ma ona implementować interfejs IRepository.
        final List<ParkingNode> nodes = repository.getParkingNodes();
        final List<ParkingNodeConnection> connections = repository.getParkingNodeConnections();
        final List<ParkingPlace> places = repository.getParkingPlaces(size);

        final CarPark carPark = new CarPark(1, nodes, connections, places);

        ParkPath path = carPark.getPath();
        List<PathNode> result = path.getPath();
        PathNode resultNode = result.get(result.size()-1);
        ((DBRepository)repository).reserveParkingPlace((int)resultNode.getX(), (int)resultNode.getY());

        return path;
    }
}