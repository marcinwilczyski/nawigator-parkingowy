package carParkModel;

import DBEntities.ParkingNode;
import DBEntities.ParkingPlace;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 31.03.2016.
 */
public class CarParkNode {
    private PathNode node;
    private boolean isParkingPlace;
    private List<CarParkNode> neighbourNodes;

    public CarParkNode(ParkingNode node){
        neighbourNodes = new ArrayList<CarParkNode>();
        this.node = new PathNode(node.x, node.y);
        this.isParkingPlace = false;
    }

    public PathNode getNode(){
        return node;
    }

    public List<CarParkNode> getNeighbourNodes(){
        return neighbourNodes;
    }

    public void addNeighbourNode(CarParkNode node){
        neighbourNodes.add(node);
    }

    public void setIsParkingPlace(boolean isParkingPlace){
        this.isParkingPlace = isParkingPlace;
    }

    public boolean isParkingPlace(){
        return isParkingPlace;
    }
}
