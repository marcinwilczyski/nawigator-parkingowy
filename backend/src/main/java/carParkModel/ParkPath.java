package carParkModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 31.03.2016.
 */
public class ParkPath {
    private List<PathNode> path;

    public ParkPath(){
        path = new ArrayList<PathNode>();
    }

    public List<PathNode> getPath(){
        return path;
    }

    public void addNode(PathNode node){
        path.add(node);
    }

    public ParkPath clone(){
        ParkPath cloned = new ParkPath();
        for(PathNode node: path){
            cloned.addNode(node);
        }
        return cloned;
    }

}
