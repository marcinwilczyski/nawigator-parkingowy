package carParkModel;

import DBEntities.*;
import javafx.util.Pair;

import java.util.*;

/**
 * Created by Marcin on 31.03.2016.
 */
public class CarPark {
    private CarParkNode firstNode;

    public CarPark(int firstNodeId, List<ParkingNode> nodes, List<ParkingNodeConnection> connections, List<ParkingPlace> places){
        HashMap<Integer, CarParkNode> carParkNodes = new HashMap<Integer, CarParkNode>();
        for (ParkingNode node:nodes) {
            carParkNodes.put(node.id, new CarParkNode(node));
        }
        firstNode = carParkNodes.get(firstNodeId);

        for (ParkingNodeConnection connection: connections){
            final CarParkNode from = carParkNodes.get(connection.fromNode);
            final CarParkNode to = carParkNodes.get(connection.toNode);
            from.addNeighbourNode(to);
        }

        for (ParkingPlace place: places){
            carParkNodes.get(place.nodeId).setIsParkingPlace(true);
        }

    }

    public ParkPath getPath() throws Exception{
        Queue<Pair<ParkPath, CarParkNode>> nodesToProcess = new LinkedList<Pair<ParkPath, CarParkNode>>();
        nodesToProcess.add(new Pair<ParkPath, CarParkNode>(new ParkPath(), firstNode));
        ParkPath result = null;
        while(!nodesToProcess.isEmpty() && result == null){
            Pair<ParkPath, CarParkNode> node = nodesToProcess.remove();
            node.getKey().addNode(node.getValue().getNode());
            if(node.getValue().isParkingPlace()){
                result = node.getKey();
            } else {
                for(CarParkNode childNode: node.getValue().getNeighbourNodes()){
                    nodesToProcess.add(new Pair<ParkPath, CarParkNode>(node.getKey().clone(), childNode));
                }
            }

        }
        if(result == null){
            throw new Exception("Brak wolnych miejsc");
        }
        return result;
    }
}
