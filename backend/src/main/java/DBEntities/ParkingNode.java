package DBEntities;

/**
 * Created by Marcin on 04.04.2016.
 */
public class ParkingNode {
    public int id;
    public float x;
    public float y;

    public ParkingNode(int id, int x, int y){
        this.id = id;
        this.x = x;
        this.y = y;
    }
}
