package DBEntities;

/**
 * Created by Marcin on 04.04.2016.
 */
public class ParkingNodeConnection {
    public int fromNode;
    public int toNode;

    public ParkingNodeConnection(int fromNode, int toNode){
        this.fromNode = fromNode;
        this.toNode = toNode;
    }
}
