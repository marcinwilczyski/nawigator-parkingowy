package DBEntities;

/**
 * Created by Marcin on 04.04.2016.
 */
public class ParkingPlace {
    
    public int id;
    public int nodeId;
    
    public ParkingPlace(int id, int nodeId){
        this.id = id;
        this.nodeId = nodeId;
    }
}
