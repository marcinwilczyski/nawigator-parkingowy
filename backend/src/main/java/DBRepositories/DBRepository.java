/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBRepositories;

import DBEntities.ParkingNode;
import DBEntities.ParkingNodeConnection;
import DBEntities.ParkingPlace;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import nawigator.ParkController;

/**
 *
 * @author F_SNCHZ
 */
public class DBRepository implements IRepository {
    
    private Connection conn = null;
    
    public void loadDriver() {
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations

            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            // handle the error
        }
    }
    
    public DBRepository() {
        DBConnect();
    }
    
    public void DBConnect() {
        loadDriver();
        
        Properties connectionProps = new Properties();
        connectionProps.put("user", "root");
        connectionProps.put("password", "");
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/navi", connectionProps);
            System.out.println("Połączono z bazą danych");
        } catch (SQLException ex) {
            Logger.getLogger(ParkController.class.getName()).log(Level.SEVERE, "nie udało się połączyć z bazą danych", ex);
            System.exit(-1);
        } 
    }

    private int getParkingPlaceIdFromCoords(int x, int y) {

        Statement stmt; ResultSet rs; int id = -1;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT PP.id FROM (ParkingPlace PP JOIN ParkingNode PN ON PP.nodeId = PN.id) WHERE PN.x=" + x + " AND PN.y=" + y);
            if (rs.next()) {
                System.out.println("Getting ParkingPlace id from coords SUCCESSFULLY");
                id = rs.getInt(1);
            }else{
                System.out.println("ERROR: Error while getting ParkingPlace id from coords!");
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

        return id;
    }

    public void reserveParkingPlace(int x, int y) {
        reserveParkingPlace(getParkingPlaceIdFromCoords(x, y));
    }

    public void releaseParkingPlace(int x, int y) {
        releaseParkingPlace(getParkingPlaceIdFromCoords(x, y));
    }

    public void reserveParkingPlace(int parkingPlaceId) {
        Statement stmt; int modifiedRowsCount = 0;
        try { 
            stmt = conn.createStatement();
            modifiedRowsCount = stmt.executeUpdate("UPDATE ParkingPlace SET isReserved = 1 WHERE id = " + parkingPlaceId);
            if (modifiedRowsCount == 1) {
                System.out.println("Parking place was reserved SUCCESSFULLY in DB");
            }else{
                System.out.println("ERROR: Error while reserving parking place in DB!");
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(DBRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void releaseParkingPlace(int parkingPlaceId) {
        Statement stmt; int modifiedRowsCount = 0;
        try { 
            stmt = conn.createStatement();
            modifiedRowsCount = stmt.executeUpdate("UPDATE ParkingPlace SET isReserved = 0 WHERE id = " + parkingPlaceId);
            if (modifiedRowsCount == 1) {
                System.out.println("Parking place was released SUCCESSFULLY in DB");
            }else{
                System.out.println("ERROR: Error while releasing parking place in DB!");
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(DBRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<ParkingNode> getParkingNodes() {
        List<ParkingNode> nodes = new ArrayList<ParkingNode>();
        
        Statement stmt; ResultSet rs;
        try { 
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT id, x, y FROM ParkingNode");
            while(rs.next()) {
                 System.out.println("New Parking Node!");
                 nodes.add(new ParkingNode(rs.getInt("id"), rs.getInt("x"), rs.getInt("y")));
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(DBRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return nodes;
    }

    @Override
    public List<ParkingNodeConnection> getParkingNodeConnections() {
        List<ParkingNodeConnection> connections = new ArrayList<ParkingNodeConnection>();
        
        Statement stmt; ResultSet rs;
        try { 
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT fromNode, toNode FROM ParkingNodeConnection");
            while(rs.next()) {
                System.out.println("New ParkingNodeConnection!");
                connections.add(new ParkingNodeConnection(rs.getInt("fromNode"), rs.getInt("toNode")));
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(DBRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

        return connections;
    }

    @Override
    public List<ParkingPlace> getParkingPlaces(String size) {

        List<ParkingPlace> places = new ArrayList<ParkingPlace>();
        
        Statement stmt; ResultSet rs;
        try { 
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT id, nodeId FROM ParkingPlace WHERE size='" + size + "' AND isReserved=0");

            while(rs.next()) {
                System.out.println("New ParkingPlace!");

                places.add(new ParkingPlace(rs.getInt("id"), rs.getInt("nodeId")));
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(DBRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

        return places;
    }

}
