package DBRepositories;

import DBEntities.ParkingNode;
import DBEntities.ParkingNodeConnection;
import DBEntities.ParkingPlace;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 20.04.2016.
 */
public class MockedRepositoryToMap implements IRepository {
    @Override
    public List<ParkingNode> getParkingNodes() {
        List<ParkingNode> nodes = new ArrayList<ParkingNode>();
        nodes.add(new ParkingNode(0, 15, 330)); //entrance

        nodes.add(new ParkingNode(1, 39, 330));

        nodes.add(new ParkingNode(2, 39, 390)); //bottom line
        nodes.add(new ParkingNode(3, 83, 390));
        nodes.add(new ParkingNode(4, 127, 390));
        nodes.add(new ParkingNode(5, 177, 390));
        nodes.add(new ParkingNode(6, 221, 390));
        nodes.add(new ParkingNode(7, 261, 390));

        nodes.add(new ParkingNode(8, 39, 444)); //bottom parking places
        nodes.add(new ParkingNode(9, 83, 444));
        nodes.add(new ParkingNode(10, 127, 444));
        nodes.add(new ParkingNode(11, 177, 444));
        nodes.add(new ParkingNode(12, 221, 444));
        nodes.add(new ParkingNode(13, 261, 444));

        return nodes;
    }

    @Override
    public List<ParkingNodeConnection> getParkingNodeConnections() {
        List<ParkingNodeConnection> connections = new ArrayList<ParkingNodeConnection>();
        connections.add(new ParkingNodeConnection(0, 1));
        connections.add(new ParkingNodeConnection(1, 2));
        connections.add(new ParkingNodeConnection(2, 3));
        connections.add(new ParkingNodeConnection(3, 4));
        connections.add(new ParkingNodeConnection(4, 5));
        connections.add(new ParkingNodeConnection(5, 6));
        connections.add(new ParkingNodeConnection(6, 7));

        connections.add(new ParkingNodeConnection(2, 8));
        connections.add(new ParkingNodeConnection(3, 9));
        connections.add(new ParkingNodeConnection(4, 10));
        connections.add(new ParkingNodeConnection(5, 11));
        connections.add(new ParkingNodeConnection(6, 12));
        connections.add(new ParkingNodeConnection(7, 13));

        return connections;
    }

    @Override
    public List<ParkingPlace> getParkingPlaces(String size) {
        List<ParkingPlace> places = new ArrayList<ParkingPlace>();
        places.add(new ParkingPlace(0, 8));
        places.add(new ParkingPlace(1, 9));
        places.add(new ParkingPlace(2, 10));
        places.add(new ParkingPlace(3, 11));
        places.add(new ParkingPlace(4, 12));
        places.add(new ParkingPlace(5, 13));
        return places;
    }
}
