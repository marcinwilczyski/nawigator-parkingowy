package DBRepositories;

import DBEntities.ParkingNode;
import DBEntities.ParkingNodeConnection;
import DBEntities.ParkingPlace;

import java.util.List;

/**
 * Created by Marcin on 20.04.2016.
 *
 * IRepository interface can be implemented for communication with database.
 */
public interface IRepository {
    /**
     * Retrieves from database and returns list of nodes in car park. Node is a place where driver has multiple choices where to drive.
     *
     * @return list of parking nodes
     */
    public List<ParkingNode> getParkingNodes();

    /**
     * Retrieves from database and returns list of connections between nodes. This connections are used to create graph, which represents car park.
     *
     * @return list of parking nodes connections
     */
    public List<ParkingNodeConnection> getParkingNodeConnections();

    /**
     * Retrieves and returns list of parking places. Each of them is connected to one parking node. Only parking places of requested size are retrieved from database.
     *
     * @param size size of requested parking place
     * @return list of parking places
     */
    public List<ParkingPlace> getParkingPlaces(String size);
}
