package DBRepositories;

import DBEntities.ParkingNode;
import DBEntities.ParkingNodeConnection;
import DBEntities.ParkingPlace;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcin on 20.04.2016.
 */
public class MockedRepository implements IRepository {
    @Override
    public List<ParkingNode> getParkingNodes() {
        List<ParkingNode> nodes = new ArrayList<ParkingNode>();
        nodes.add(new ParkingNode(0, 0, 0));
        nodes.add(new ParkingNode(1, 0, 1));
        nodes.add(new ParkingNode(2, 0, 2));
        nodes.add(new ParkingNode(3, 0, 3));

        nodes.add(new ParkingNode(7, 1, 1));

        nodes.add(new ParkingNode(4, -1, 3));
        nodes.add(new ParkingNode(5, 1, 3));
        nodes.add(new ParkingNode(6, 0, 4));
        return nodes;
    }

    @Override
    public List<ParkingNodeConnection> getParkingNodeConnections() {
        List<ParkingNodeConnection> connections = new ArrayList<ParkingNodeConnection>();
        connections.add(new ParkingNodeConnection(0, 1));
        connections.add(new ParkingNodeConnection(1, 2));
        connections.add(new ParkingNodeConnection(2, 3));

        connections.add(new ParkingNodeConnection(3, 4));
        connections.add(new ParkingNodeConnection(3, 5));
        connections.add(new ParkingNodeConnection(3, 6));

        connections.add(new ParkingNodeConnection(1, 7));
        return connections;
    }

    @Override
    public List<ParkingPlace> getParkingPlaces(String size) {
        List<ParkingPlace> places = new ArrayList<ParkingPlace>();
        places.add(new ParkingPlace(0, 4));
        places.add(new ParkingPlace(1, 5));
        places.add(new ParkingPlace(2, 6));
        places.add(new ParkingPlace(3, 7));
        return places;
    }
}
