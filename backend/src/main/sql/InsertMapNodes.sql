/*
===================================================
--> Parking Nodes Coords
===================================================
*/

INSERT INTO ParkingNode (x, y) VALUES

(18, 331),	 /* wjazd */
(55, 331),	 /* przed wjazdem */
(118, 331),	 /* pierwsze MP */

(55, 271),	 /* wezel na lewo od wjazdu */	
(118, 271),	 /* MP */
(55, 215),	 /* wezel */
(118, 215),	 /* MP */
(55, 158),	 /* wezel */
(118, 158),	 /* MP */
	
(55, 391),	 /* wezel na prawo od wjazdu, lewy dolny skr�t */
(36, 447),	 /* MP lewy dolny r�g */
(81, 391),	 /* wezel */
(81, 447),	 /* MP */
(126, 391),	 /* wezel */
(126, 447),	 /* MP */
(173, 391),	 /* wezel */
(173, 447),	 /* MP */
(219, 391),	 /* wezel */
(219, 447),	 /* MP */
(244, 391),	 /* wezel prawy dolny skr�t */
(260, 447),	 /* MP prawy dolny r�g */
(244, 331),	 /* wezel */
(177, 331),	 /* MP powi�kszone */
(244, 271),	 /* wezel */
(177, 271),	 /* MP powi�kszone */
(244, 215),	 /* wezel */
(177, 215),	 /* MP powi�kszone */
(244, 158),	 /* wezel */
(177, 158),	 /* MP powi�kszone */
(244, 98),	 /* wezel prawy g�rny skr�t */
(260, 34),	 /* MP prawy g�rny r�g */
(219, 98),	 /* wezel */
(219, 34),	 /* MP */
(173, 98),	 /* wezel */
(173, 34),	 /* MP */
(126, 98),	 /* wezel */
(126, 34),	 /* MP */
(81, 98),	 /* wezel */
(81, 34),	 /* MP */
(36, 98),	 /* wezel */
(36, 34)	 /* MP lewy g�rny r�g */

;




/*
===================================================
--> Parking Node's Connections
===================================================
*/

INSERT INTO ParkingNodeConnection VALUES

(1, 2),
(2, 3),
(2, 4),
(4, 5),
(4, 6),
(6, 7),
(6, 8),
(8, 9),

(2, 10),
(10, 11),
(10, 12),
(12, 13),
(12, 14),
(14, 15),
(14, 16),
(16, 17),
(16, 18),
(18, 19),
(18, 20),
(20, 21),
(20, 22),
(22, 23),
(22, 24),
(24, 25),
(24, 26),
(26, 27),
(26, 28),
(28, 29),
(28, 30), /* 30 to prawy g�rny skr�t */
(30, 31),
(30, 32),
(32, 33),
(32, 34),
(34, 35),
(34, 36),
(36, 37),
(36, 38),
(38, 39),
(38, 40),
(40, 41) /* 41 to ostatnie MP, lewy g�rny r�g */

;




/*
===================================================
--> Parking Places With Nodes Associate
===================================================
*/

INSERT INTO ParkingPlace (nodeId, size) VALUES

(3, 'normal'),
(5, 'normal'),
(7, 'normal'),
(9, 'normal'),
(11, 'normal'),
(13, 'normal'),
(15, 'normal'),
(17, 'normal'),
(19, 'normal'),
(21, 'normal'),
(23, 'large'),
(25, 'large'),
(27, 'large'),
(29, 'large'),
(31, 'normal'),
(33, 'normal'),
(35, 'normal'),
(37, 'normal'),
(39, 'normal'),
(41, 'normal')

;