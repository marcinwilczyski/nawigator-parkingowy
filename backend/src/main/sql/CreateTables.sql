DROP TABLE IF EXISTS ParkingPlace;
DROP TABLE IF EXISTS ParkingNodeConnection;
DROP TABLE IF EXISTS ParkingNode;


CREATE TABLE ParkingNode (

	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	x INT NOT NULL,
	y INT NOT NULL

) ENGINE=INNODB;


CREATE TABLE ParkingNodeConnection (

	fromNode INT(6) UNSIGNED NOT NULL,
	toNode INT(6) UNSIGNED NOT NULL,
	FOREIGN KEY (fromNode) REFERENCES ParkingNode(id),
	FOREIGN KEY (toNode) REFERENCES ParkingNode(id)

) ENGINE=INNODB;


CREATE TABLE ParkingPlace (

	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	nodeId INT(6) UNSIGNED NOT NULL,
	size ENUM('normal', 'large'),
	isReserved boolean not null default 0,
	FOREIGN KEY (nodeId) REFERENCES ParkingNode(id)

) ENGINE=INNODB;
